import { Entity, Column, ObjectID, ObjectIdColumn } from 'typeorm';

@Entity()
export class Setting {
  @ObjectIdColumn()
  id: ObjectID;

  @Column({ unique: true })
  key: string;

  @Column()
  data: any;
}