import { Entity, Column, BeforeInsert, BeforeUpdate, ObjectIdColumn, ObjectID } from "typeorm";
import {
  IsString, IsNotEmpty, Min, Max, IsNumber, validateOrReject
} from 'class-validator';

export type Stok = 'Full' | 'Half' | 'None';
export type SistemPembayaran = 'Cepat' | 'Sedang' | 'Lama';

@Entity()
export class Supplier {
  @ObjectIdColumn()
  id: ObjectID;

  @IsString()
  @IsNotEmpty()
  @Column()
  nama: string;

  @IsNumber()
  @Min(2000)
  @Column()
  harga: number;

  @IsNumber()
  @Min(0)
  @Max(15)
  @Column()
  diskon: number;

  @IsNumber()
  @Min(1)
  @Column()
  expire: number;

  @Column()
  stok: Stok;

  @IsNumber()
  @Min(0)
  @Column()
  jarak: number;

  @Column()
  sistemPembayaran: SistemPembayaran;

  @BeforeInsert()
  @BeforeUpdate()
  private async validate() {
    return validateOrReject(this);
  }
}

