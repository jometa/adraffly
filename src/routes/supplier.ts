import { FastifyInstance } from 'fastify';
import { Supplier } from '@adraffy/entities/supplier';
import { getMongoRepository } from 'typeorm';
import { ObjectID } from 'mongodb';

export default async function(fastify: FastifyInstance) {

  const suppliers = getMongoRepository(Supplier);

  fastify.post('/', async (req, rep) => {
    let supplier = suppliers.create(req.body);
    supplier = await suppliers.save(supplier);
    return supplier;
  });

  fastify.get('/', async (req, rep) => {
    return suppliers.find();
  });

  fastify.get('/:id', async (req, rep) => {
    const supplier = await suppliers.findOneOrFail(req.params.id);
    return supplier;
  });

  fastify.delete('/:id', async (req, rep) => {
    let supplier = await suppliers.findOneOrFail(req.params.id);
    await suppliers.remove(supplier);
    rep.send('OK');
  });

  fastify.put('/:id', {
    schema: {
      body: {
        type: 'object',
        props: {
          diskon: {
            type: 'number'
          },
          harga: {
            type: 'number'
          },
          stok: {
            type: 'string'
          },
          expire: {
            type: 'number'
          },
          jarak: {
            type: 'number'
          },
          sistemPembayaran: {
            type: 'number'
          }
        },
        required: ['diskon', 'harga', 'expire', 'jarak', 'stok', 'sistemPembayaran']
      }
    },
    handler: async (req, rep) => {
      let supplier = await suppliers.findOneOrFail(req.params.id);
      supplier.harga = req.body.harga;
      supplier.diskon = req.body.diskon;
      supplier.jarak = req.body.jarak;
      supplier.sistemPembayaran = req.body.sistemPembayaran;
      supplier.stok = req.body.stok;
      await suppliers.save(supplier);
      rep.send('OK');
    }
  });
}