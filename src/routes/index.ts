const carbone = require('carbone');
const moment = require('moment');

import { writeFileSync, readFileSync }  from 'fs';
import { FastifyInstance } from "fastify"
import supplierRoutes from './supplier'
import settingRoutes from './settings'

export default async function (fastify: FastifyInstance) {
  fastify
    .register(supplierRoutes, { prefix: '/api/suppliers' })
    .register(settingRoutes, { prefix: '/api/settings' })
  
  const TEMPLATE_PATH = './templates/reports'; 
  const TEMPLATE_FILE_PATH = `${TEMPLATE_PATH}/ranks.docx`;
  fastify.post('/api/reports', {
    handler: async (request, reply) => {
      const items = request.body.items;
      const dataToPrint = {
        items,
        waktu: moment().format("dddd, MMMM Do YYYY, h:mm:ss a"),
        total: items.length
      };

      const prom = new Promise<string>((resolve, reject) => {
        carbone.render(TEMPLATE_FILE_PATH, dataToPrint, function(err, result) {
          if (err) {
            console.log(err);
            reject(err);
          }
          // write the result
          writeFileSync('report.docx', result);
          resolve('report.docs')
        });
      });

      prom.then(result => {
        const buffer = readFileSync(result)
        reply
          .type('application/vnd.openxmlformats-officedocument.wordprocessingml.document')
          .send(buffer);
      })
    }
  })
}