import "reflect-metadata";
import fp from "fastify-plugin";
import { Connection, EntityManager, createConnection, getMongoManager } from "typeorm";
import { FastifyInstance } from "fastify";

export interface Torm {
  connection: Connection;
}

declare module "fastify" {
  interface FastifyInstance {
    torm: Torm;
  }
}

async function plugin(fastify: FastifyInstance, tormConfig: any) {

  const connection = await createConnection(tormConfig);
  fastify.decorate('torm', {
    connection
  });
};

export default fp(plugin);
