require('dotenv').config();

import "reflect-metadata";

import appFunction from "./app";

const config = {
  orm: {
    type: 'mongodb',
    host: 'localhost',
    database: 'adraffly',
    port: 27017,
    entities: ['dist/entities/*.js'],
    logging: process.env.TYPEORM_LOGGING == 'true'
  }
};

(async function () {
  const app = await appFunction(config);
  try {
    await app.listen(3000)
    app.blipp();
  } catch (err) {
    console.log(err)
    process.exit(1)
  }
})()