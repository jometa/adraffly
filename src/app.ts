import Fastify from 'fastify';
import blipp from 'fastify-blipp';
import cors from 'fastify-cors';
import torm from './plugins/torm';
import routes from './routes';

export default async function (config: any) {
  const fastify = Fastify({
    logger: true
  });

  fastify.register(cors);
  fastify.register(blipp);
  fastify.register(torm, config.orm);
  fastify.register(routes);

  return fastify;
}

