"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require('dotenv').config();
require("reflect-metadata");
const app_1 = __importDefault(require("./app"));
const config = {
    orm: {
        type: 'mongodb',
        host: 'localhost',
        database: 'adraffly',
        port: 27017,
        entities: ['dist/entities/*.js'],
        logging: process.env.TYPEORM_LOGGING == 'true'
    }
};
(async function () {
    const app = await app_1.default(config);
    try {
        await app.listen(3000);
        app.blipp();
    }
    catch (err) {
        console.log(err);
        process.exit(1);
    }
})();
//# sourceMappingURL=index.js.map