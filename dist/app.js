"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fastify_1 = __importDefault(require("fastify"));
const fastify_blipp_1 = __importDefault(require("fastify-blipp"));
const fastify_cors_1 = __importDefault(require("fastify-cors"));
const torm_1 = __importDefault(require("./plugins/torm"));
const routes_1 = __importDefault(require("./routes"));
async function default_1(config) {
    const fastify = fastify_1.default({
        logger: true
    });
    fastify.register(fastify_cors_1.default);
    fastify.register(fastify_blipp_1.default);
    fastify.register(torm_1.default, config.orm);
    fastify.register(routes_1.default);
    return fastify;
}
exports.default = default_1;
//# sourceMappingURL=app.js.map