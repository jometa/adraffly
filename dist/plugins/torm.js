"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("reflect-metadata");
const fastify_plugin_1 = __importDefault(require("fastify-plugin"));
const typeorm_1 = require("typeorm");
async function plugin(fastify, tormConfig) {
    const connection = await typeorm_1.createConnection(tormConfig);
    fastify.decorate('torm', {
        connection
    });
}
;
exports.default = fastify_plugin_1.default(plugin);
//# sourceMappingURL=torm.js.map