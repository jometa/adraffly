/// <reference path="torm.d.ts" />
/// <reference types="node" />
/// <reference types="fastify-blipp" />
import "reflect-metadata";
import { Connection } from "typeorm";
import { FastifyInstance } from "fastify";
export interface Torm {
    connection: Connection;
}
declare module "fastify" {
    interface FastifyInstance {
        torm: Torm;
    }
}
declare const _default: (instance: FastifyInstance<import("http").Server, import("http").IncomingMessage, import("http").ServerResponse>, options: any, callback: (err?: import("fastify").FastifyError) => void) => void;
export default _default;
