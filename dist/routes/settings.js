"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const setting_1 = require("@adraffy/entities/setting");
async function default_1(fastify) {
    const settings = typeorm_1.getMongoRepository(setting_1.Setting);
    fastify.post('/', async (req, rep) => {
        let supplier = settings.create(req.body);
        const total = await settings.count({
            key: req.body.key
        });
        if (total > 0) {
            rep.status(500).send('Already Exists');
            return;
        }
        supplier = await settings.save(supplier);
        return supplier;
    });
    fastify.get('/', async (req, rep) => {
        return settings.find();
    });
    fastify.get('/:key', async (req, rep) => {
        return settings.findOneOrFail({ key: req.params.key });
    });
    fastify.delete('/:key', async (req, rep) => {
        let setting = await settings.findOneOrFail({
            key: req.params.key
        });
        await settings.remove(setting);
        rep.send('OK');
    });
    fastify.put('/:key', {
        handler: async (req, rep) => {
            let setting = await settings.findOneOrFail({
                key: req.params.key
            });
            setting.data = req.body;
            await settings.save(setting);
            rep.send('OK');
        }
    });
}
exports.default = default_1;
//# sourceMappingURL=settings.js.map