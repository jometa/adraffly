"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const carbone = require('carbone');
const moment = require('moment');
const supplier_1 = __importDefault(require("./supplier"));
const settings_1 = __importDefault(require("./settings"));
async function default_1(fastify) {
    fastify
        .register(supplier_1.default, { prefix: '/api/suppliers' })
        .register(settings_1.default, { prefix: '/api/settings' });
    const TEMPLATE_PATH = './templates/reports';
    const TEMPLATE_FILE_PATH = `${TEMPLATE_PATH}/ranks.docx`;
    fastify.post('/api/reports', {
        handler: async (request, reply) => {
            const items = request.body.items;
            const dataToPrint = {
                items,
                waktu: moment().format("dddd, MMMM Do YYYY, h:mm:ss a"),
                total: items.length
            };
            const prom = new Promise((resolve, reject) => {
                carbone.render(TEMPLATE_FILE_PATH, dataToPrint, function (err, result) {
                    if (err) {
                        console.log(err);
                        reject(err);
                    }
                    resolve(result);
                });
            });
            prom.then(result => {
                reply
                    .type('application/vnd.openxmlformats-officedocument.wordprocessingml.document')
                    .send(result);
            });
        }
    });
}
exports.default = default_1;
//# sourceMappingURL=index.js.map