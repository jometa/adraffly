/// <reference types="node" />
import Fastify from 'fastify';
export default function (config: any): Promise<Fastify.FastifyInstance<import("http").Server, import("http").IncomingMessage, import("http").ServerResponse>>;
