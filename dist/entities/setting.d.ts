import { ObjectID } from 'typeorm';
export declare class Setting {
    id: ObjectID;
    key: string;
    data: any;
}
