import { ObjectID } from "typeorm";
export declare type Stok = 'Full' | 'Half' | 'None';
export declare type SistemPembayaran = 'Cepat' | 'Sedang' | 'Lama';
export declare class Supplier {
    id: ObjectID;
    nama: string;
    harga: number;
    diskon: number;
    expire: number;
    stok: Stok;
    jarak: number;
    sistemPembayaran: SistemPembayaran;
    private validate;
}
