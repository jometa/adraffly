"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const class_validator_1 = require("class-validator");
let Supplier = class Supplier {
    async validate() {
        return class_validator_1.validateOrReject(this);
    }
};
__decorate([
    typeorm_1.ObjectIdColumn(),
    __metadata("design:type", typeorm_1.ObjectID)
], Supplier.prototype, "id", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    typeorm_1.Column(),
    __metadata("design:type", String)
], Supplier.prototype, "nama", void 0);
__decorate([
    class_validator_1.IsNumber(),
    class_validator_1.Min(2000),
    typeorm_1.Column(),
    __metadata("design:type", Number)
], Supplier.prototype, "harga", void 0);
__decorate([
    class_validator_1.IsNumber(),
    class_validator_1.Min(0),
    class_validator_1.Max(15),
    typeorm_1.Column(),
    __metadata("design:type", Number)
], Supplier.prototype, "diskon", void 0);
__decorate([
    class_validator_1.IsNumber(),
    class_validator_1.Min(1),
    typeorm_1.Column(),
    __metadata("design:type", Number)
], Supplier.prototype, "expire", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], Supplier.prototype, "stok", void 0);
__decorate([
    class_validator_1.IsNumber(),
    class_validator_1.Min(0),
    typeorm_1.Column(),
    __metadata("design:type", Number)
], Supplier.prototype, "jarak", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], Supplier.prototype, "sistemPembayaran", void 0);
__decorate([
    typeorm_1.BeforeInsert(),
    typeorm_1.BeforeUpdate(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], Supplier.prototype, "validate", null);
Supplier = __decorate([
    typeorm_1.Entity()
], Supplier);
exports.Supplier = Supplier;
//# sourceMappingURL=supplier.js.map